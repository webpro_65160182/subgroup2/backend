import { IsArray, IsNotEmpty } from "class-validator";

export class CreateEmployeeDto {

    @IsNotEmpty()
    name: string;

    @IsNotEmpty() //ห้ามว่าง
    tel: string;

    // @IsArray() //ต้องใส่เป็นArrayเท่านั้น
    // status: ('PartTime' | 'FullTime')[];

    @IsNotEmpty()
    salary: number;

    @IsNotEmpty()
    dateIn: string;

    @IsNotEmpty()
    checkIn: string;

    @IsNotEmpty()
    checkOut: string;
}
