import { Column, Entity, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Employee {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    tel: string;


    // status: ('PartTime' | 'FullTime')[];

    @Column()
    salary: number;

    @Column()
    dateIn: string;

    @Column()
    checkIn: string;

    @Column()
    checkOut: string;
}
